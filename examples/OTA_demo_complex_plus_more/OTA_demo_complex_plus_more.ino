#include <ESP8266WiFi.h>
#include <ArduinoOTA.h>

byte gLedStatus = LOW;
#define TOGGLE_LED     digitalWrite(LED_BUILTIN,gLedStatus = (gLedStatus == HIGH) ? LOW : HIGH );
#define LED_ON  digitalWrite(LED_BUILTIN, gLedStatus = LOW);
#define LED_OFF digitalWrite(LED_BUILTIN, gLedStatus = HIGH);

int connectWiFi(const char *WiFiSSID, const char *WiFiPSK, int WiFiChannel=0)
{
  uint16_t connectCount = 0;
  
  int WiFiStatus;

  // Set WiFi mode to station (as opposed to AP or AP_STA)
  WiFi.mode(WIFI_STA);

  // WiFI.begin([ssid], [passkey]) initiates a WiFI connection
  // to the stated [ssid], using the [passkey] as a WPA, WPA2,
  // or WEP passphrase.
  Serial.print(WiFiSSID);
  Serial.print('/');
  Serial.print(WiFiPSK);
  Serial.print(':');
  Serial.println(WiFiChannel);
  
  WiFiStatus = WiFi.begin(WiFiSSID, WiFiPSK, WiFiChannel);
  Serial.print("WiFi.begin returns: ");
  Serial.println(WiFiStatus);

  // Use the WiFi.status() function to check if the ESP8266
  // is connected to a WiFi network.

  while ( (connectCount < 256) && ( WL_CONNECTED != ( WiFiStatus = WiFi.status() )  ) )
  {
    // Blink the LED
    TOGGLE_LED ;
  
    // Delays allow the ESP8266 to perform critical tasks
    // defined outside of the sketch. These tasks include
    // setting up, and maintaining, a WiFi connection.
    delay(50);
    // Potentially infinite loops are generally dangerous.
    // Add delays -- allowing the processor to perform other
    // tasks -- wherever possible.
    if ( !connectCount ) 
    {
      Serial.print("WiFiStatus: ");
    }
    connectCount++;
    Serial.print(WiFiStatus);
    Serial.print(' ');
  }
  Serial.println("");
  if ( WiFiStatus == WL_CONNECTED )
  {
    Serial.println("WiFi connected!");
  }
  
  return WiFiStatus;
}

void myYield()
{
  yield();
  ArduinoOTA.handle();
  yield();
  /* this is a good place to call webserver.handleClient() 
   * if you're using the traditional esp8266 webserver library
   */
}

void myDelay(unsigned long ms) 
{
   unsigned long start = millis();
  while ( millis() - start < ms )
  {
    myYield();
    /* could optionally put in a short delay here
     * but how short
     * is battery life a concern to you?
     */
  }
}

void setup() {
  Serial.begin(74880);
  Serial.println("\nBooting");
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(A0, INPUT);
  pinMode(D1, INPUT);

  randomSeed(analogRead(A0));

  connectWiFi("SlootNet","12345678");

  ArduinoOTA.setHostname("DemoEsp8266");
  ArduinoOTA.setPassword("12345678");
  
  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  randomSeed(analogRead(A0));
}


void loop ()
{
  myYield();  // often

// do all your magical things in here that take a longer length of time 
// loops, pushing long strings to a ws2812 chain, whatever
// just remember to call myYield often

  for (int j=0; j < 5 ; j++)
  {
    LED_ON;
    
    myDelay(1000); 
  
    LED_OFF;
  
    myDelay(1000); 
  }

  myYield();

  for (int i=0 ; i < 50 ; i++ )
    {
      TOGGLE_LED;
      myDelay(random(5,50)*10);
    }

  myYield();  // often
}

